<?php
include('Smartcache.php');

$cache_config = array(
    'container' => 'File',
    // 'ttl' => 3600,
    'ttl' => 10,
    'File' => array(
        'store' => DEFAULT_BASEPATH.'cache',
        'auto_clean'      => 100, 
        'auto_clean_life' => 3600,                         
        'auto_clean_all'  => false,
        'max_estimate_process' =>  1,                        
        'debug'  => true
    )
);

$smartcache = new Smartcache($cache_config);

$key = $smartcache->generatekey('test');

if (($html = $smartcache->fetch($key)) === false){

    $html = file_get_contents('http://www.timeapi.org/utc/now');

    $smartcache->store($key, $html);
}

echo $html;
echo '<br/>';
echo time();
